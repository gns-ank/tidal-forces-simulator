Gravitational simulator
===

This is a general gravitational force calculator, which I have produced as a part of my `C++` assignment during my third year of physics studies.
The project also contains a very simple test suite, which I have written on my own to check for bugs/regressions in the code.

Usage
===

To run the program run `make` in the base directory
and it will create required directories and will compile the program.
Afterwards, a binary will be created by the name of `program`.
It will forward the output into the current directory which was created by make.
A command `make runall` will compile the code and run the simulations.
Plotting and video generation is done with `make plotall` command.
There are some directories needed, which will be created whilst
compiling the program by executing `make`.
`make test` will compile the code and run the test suite.

Requirements
===

The GSL library and several C++ STL are used. Plotting is done using
Matplotlib Python 3 interface, which works on the MCS machines the last
time I checked it. To generate video, ffmpeg must be present.

<!-- vim: tw=72 spell spelllang=en_gb
