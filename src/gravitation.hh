/*
 * This is a header file containing the particle class, which will be generally either
 * the test mass, or the gravitating body
 */

#ifndef __GRAV_HH_X_
#define __GRAV_HH_X_

#include <vector>
#include "vec3.hh"

namespace grav {
    struct experimentContainer {
        double t, tMax, dt, G;
        int expid, frame;
        std::string datadir;
        bool getConservedQ;
        double range;
    };

    class mParticle {
        public:
            double mMass;
            cav::Vec3 mPositionCurrent,
                mPositionNext,
                mVelocity,
                mAcceleration;

            bool mTest, mSkipable;

            bool isTest () const { return mTest; }
            bool skip () const { return mSkipable; }

    };
    class ParticleSystem {
        private:

            std::vector<mParticle> mSystem;
            bool mVerletInit;
        public:
            // Constructors
            ParticleSystem () { mVerletInit = true; }

            // Destructor
            ~ParticleSystem () {}

            // the size method might be useful
            unsigned size () { return mSystem.size(); }
            void    clear () { mSystem.clear(); }

            // Add a particles
            int  addParticle (double, cav::Vec3, cav::Vec3);
            void addTestParticles (unsigned, double, double, unsigned);

            // Calculate acceleration for all particles at once
            void setAcceleration (experimentContainer&);

            // Solving ODEs: time evolution
            void evolveEuler  (experimentContainer&);
            void evolveVerlet (experimentContainer&);

            // We need some methods for data outputting and they need to be formating
            // independent.
            std::vector<cav::Vec3> getAccelerations () const;
            std::vector<cav::Vec3> getVelocities () const;
            std::vector<cav::Vec3> getPositions () const;
            std::vector<cav::Vec3> getEccentricities (experimentContainer&) const;
            std::vector<cav::Vec3> getAngularMomentum () const;
            cav::Vec3              getTotalAngularMomentum () const;
            double                 getTotalEnergy (experimentContainer&) const;

            // Cleanup the particles if they flow out of the range of interest
            void cleanup (experimentContainer& params);
    };
}

#endif
