/*
 * This is the implementation of the header file.
 * the test mass, or the gravitating body
 */

#include <cmath>
#include "vec3.hh"
#include "gravitation.hh"
#include <algorithm>

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

namespace grav {
    int ParticleSystem::addParticle (double m, cav::Vec3 r, cav::Vec3 v) {
        mParticle p;
        p.mMass = m;
        p.mTest = (m == 0);
        p.mPositionCurrent = r;
        p.mVelocity = v;
        p.mSkipable = false;

        mSystem.push_back(p);
        
        // Return the index integer of the newly created particle
        return mSystem.size() - 1;
    }

    // Net acceleration of a particle
    void ParticleSystem::setAcceleration (experimentContainer& params) {

        // This double loop goes through all possible pairs only once
        for (unsigned i = 0; i < mSystem.size(); i++) {
            // Ensure that the acceleration is reset to zero before calculating
            mSystem[i].mAcceleration = cav::Vec3(0,0,0);

            // If the particle is out of bounds, continue
            if (mSystem[i].skip()) {
                continue;
            }

            // Not using Newton's 3rd law because it will make the code look ugly and it
            // will cut the calculations only by half at best. Not worth it.
            for (unsigned j = 0; j < mSystem.size(); j++) {
                // Do not do anything if it is a test particle, or if the particles are
                // the same.
                if (mSystem[j].isTest() or j==i) {
                    continue;
                }
                
                // Calculate the displacement vector and the acceleration in an
                // inverse square law.
                cav::Vec3 r = mSystem[j].mPositionCurrent - mSystem[i].mPositionCurrent;
                mSystem[i].mAcceleration += mSystem[j].mMass * r / pow(r.getLength(), 3);
            }

            // Multiply by coupling strength to get units right
            mSystem[i].mAcceleration *= params.G;
        }

        // Now do transform the acceleration so that we are in the right frame:
        if (params.frame > -1 and params.frame < (int) mSystem.size() 
               and not (mSystem[params.frame].mAcceleration == cav::Vec3(0,0,0)) ) {
            for (unsigned i = 0; i < mSystem.size(); i++) {
                mSystem[i].mAcceleration -= mSystem[params.frame].mAcceleration;
            }
        }
    }

    // Add test particles
    void ParticleSystem::addTestParticles (unsigned idx, double R, double GM, unsigned N) {
        // Add all the particles in a circle with equal distances ro each other
        for (unsigned i = 0; i < N; i++) {
            // Calculate sines and cosines to decrease the number of computations
            // slightly
            const double cos_val = cos(i*2*M_PI/N), 
                         sin_val = sin(i*2*M_PI/N);
            // Construct a position and velocity vectors;
            cav::Vec3 r (R*cos_val, R*sin_val,0);
            cav::Vec3 v ( -sin_val, cos_val, 0);
            v *= pow(GM/R, 0.5);

            // Account for movement and position of the gravitating body, such that
            // the particles are in circular trajectories in the bodies frame.
            v += mSystem[idx].mVelocity;
            r += mSystem[idx].mPositionCurrent;
            
            // Push back into the input array
            ParticleSystem::addParticle (0, r, v);
        }
    }

    // Euler evolution
    void ParticleSystem::evolveEuler (experimentContainer& params) {
        double h = params.dt;

        // Set accelerations
        ParticleSystem::setAcceleration (params);

        for (unsigned i = 0; i < mSystem.size(); i++) {
            if ((int) i != params.frame) {
                mSystem[i].mVelocity += h * mSystem[i].mAcceleration;
                mSystem[i].mPositionCurrent += h * mSystem[i].mVelocity;
            } else {
                mSystem[i].mVelocity = cav::Vec3(0, 0, 0);
            }
        }

        // Ensure that the mVerlet Init is true;
        mVerletInit = true;
    }

    // Verlet evolution
    void ParticleSystem::evolveVerlet (experimentContainer& params) {
        double h = params.dt;

        // Initialize the required quantities for the method using Euler Integration
        if (mVerletInit) {
            for (unsigned i = 0; i < mSystem.size(); i++) {
                mSystem[i].mPositionNext = mSystem[i].mPositionCurrent;
                // this is the frame, then we have no velocity
                if (params.frame != (int) i) {
                    mSystem[i].mPositionNext += h * mSystem[i].mVelocity;
                }
            }

            mVerletInit = false;
        }

        // Create a temp array which store the previous position
        std::vector<cav::Vec3> tmp;
        tmp.resize(mSystem.size());

        // There was a potential bug by using a[i-1] as a[i]
        for (unsigned i = 0; i < mSystem.size(); i++) {
            tmp[i] = mSystem[i].mPositionCurrent;
            mSystem[i].mPositionCurrent = mSystem[i].mPositionNext;
        }

        // Set the accelerations only now
        ParticleSystem::setAcceleration (params);

        // There was a potential bug by using a[i-1] as a[i]
        for (unsigned i = 0; i < mSystem.size(); i++) {
            if (params.frame == (int) i or mSystem[i].skip()) {
                continue;
            }

            // mPositionCurrent is the same as mPosition Next. I can avoid
            // multiplication here
            mSystem[i].mPositionNext += mSystem[i].mPositionCurrent - tmp[i]
                + pow(h,2) * mSystem[i].mAcceleration;

            mSystem[i].mVelocity = ( mSystem[i].mPositionNext - tmp[i] ) / (2*h);

            // This is a quick hack where we ignore particles too far away. For proper
            // usage, we should implement a method where we pass the range
            if (mSystem[i].mPositionCurrent.getLength() > params.range) {
                mSystem[i].mSkipable = true;
            }

        }

    }

    // Outputing methods:
    std::vector<cav::Vec3> ParticleSystem::getAccelerations () const {
        std::vector<cav::Vec3> a;

        for (unsigned i=0; i < mSystem.size(); i++) {
                a.push_back (mSystem[i].mAcceleration);
        }
        
        return a;
    }

    std::vector<cav::Vec3> ParticleSystem::getVelocities () const {
        std::vector<cav::Vec3> v;

        for (unsigned i=0; i < mSystem.size(); i++) {
            v.push_back (mSystem[i].mVelocity);
        }
        
        return v;
    }

    std::vector<cav::Vec3> ParticleSystem::getPositions () const {
        std::vector<cav::Vec3> x;

        for (unsigned i=0; i < mSystem.size(); i++) {
            x.push_back (mSystem[i].mPositionCurrent);
        }
        
        return x;
    }

    // Conserved quantities:
    // It is much easier to calculate them on demmand, hence the output methods are
    // implementing the calculation as well.
    cav::Vec3 ParticleSystem::getTotalAngularMomentum () const {
        cav::Vec3 J;

        for (unsigned i=0; i < mSystem.size(); i++) {
            // The formula is J = p \times r = m v \times r
            if (mSystem[i].isTest()) {
                continue;
            }
            J += mSystem[i].mMass * mSystem[i].mPositionCurrent * mSystem[i].mVelocity;
        }

        return J;
    }

    double ParticleSystem::getTotalEnergy (experimentContainer& params) const {
        double E = 0;

        for (unsigned i=0; i < mSystem.size(); i++) {
            // Test masses do have any energy
            if (mSystem[i].isTest()) {
                continue;
            }
            // Kinetic Energy contribution
            E += 0.5 * mSystem[i].mMass * mSystem[i].mVelocity.getLengthSquared();

            // Potential Energy contribution. Invariant under Galilean transformations
            for (unsigned j = 0; j < mSystem.size(); j++) {
                if (mSystem[j].isTest() or j == i) {
                    continue;
                }

                const double r = (mSystem[i].mPositionCurrent - mSystem[j].mPositionCurrent).getLength();

                // The factor of 2 in the denominator is for accounting double counting.
                E -= 0.5 * params.G * mSystem[i].mMass * mSystem[j].mMass / r;
            }
        }

        return E;
    }
    
    // The eccentricity vectors could be also calculated, and this is indeed related to
    // the Laplace-Runge-Lenz vector, but this is meaningful only in a simple two body
    // problem. Whether this will be useful, we'll see in the future.
    //
    // Some info: http://en.wikipedia.org/wiki/Eccentricity_vector
    //
    // FIXME: Adjust the inside function so that we have proper handling of Gallilean
    // transformations.
    std::vector<cav::Vec3> ParticleSystem::getEccentricities (experimentContainer& params) const {
        std::vector<cav::Vec3> e;
        double mu = mSystem[params.frame].mMass * params.G;

        for (unsigned i=0; i < mSystem.size(); i++) {
            // Galilean Transformations
            cav::Vec3 v = mSystem[i].mVelocity;
            cav::Vec3 r = mSystem[i].mPositionCurrent;

            e.push_back ( ( r * v.getLengthSquared()  - v * ( r % v ) ) / mu
                    - cav::normalise (r));
        }
        
        return e;
    }

}
