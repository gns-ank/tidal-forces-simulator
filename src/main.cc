/*
 * This is the main program for the gravitational simulation using the Verlet algorithm.
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>
#include <stdlib.h>

#include "vec3.hh"
#include "gravitation.hh"

// Functions for unit tests
#include "test_gravitation.hh"

int testMain ();

int experimentWrapper (grav::ParticleSystem&, grav::experimentContainer);
int experimentKepler1 (grav::experimentContainer);
int experimentTestMasses (grav::experimentContainer);
int experimentParabolic (grav::experimentContainer);
int experimentParabolicWithTestMasses (grav::experimentContainer);

// Use the constants from the cmath library
#define _USE_MATH_DEFINES

// Make use of parameter passing to the executable
// Possible parameters are:
//      testonly, test
int main (int argc, char *argv[]) {
    std::cout << "Tidal forces between interacting galaxies project.\n" << std::endl;
    std::vector<int> expids;
    int shift = 0;

    if ( argv[1] == std::string("test") ) {
        int ret = testMain();
        shift++;

        if (ret != 0) {
            std::cout << "The tests had not been passed, aborting." << std::endl;
            return ret;
        }
    }

    if ( argc > 2+shift and argv[1+shift] == std::string("run") ) {
        for (int i = 2+shift; i < argc; i ++) {
            char * pEnd;
            // FIXME: ideally I need a check here
            int idx = strtol(argv[i], &pEnd, 10);
            if (pEnd != argv[i]) {
                expids.push_back(idx);
            } else {
                std::cout << "WARNING: the id " << argv[i] << " is not a valid integer."
                    << "Skipping this parameter"
                    << std::endl;
            }
        }
    } else if (shift != 1 or argc == 1) {
        std::cout << "Too many parameters given to the executable.\n"
            << "The supported parameters:\n"
            << "    - 'test' run the tests before the main program.\n"
            << "    - 'run' run only the selected experiments. Enter the experiment list as a list of integers separated by spaces.\n"
            << "    - any other parameter will print this message.\n"
            << "\n"
            << "Examples:\n"
            << "   " << argv[0] << " test run 1 2 3\n"
            << "   " << argv[0] << " test\n"
            << "   " << argv[0] << " run 2 3\n"
            << std::endl;
    }

    // Some default parameters for the experiments.
    grav::experimentContainer params;
    params.G = 1;
    params.t = 0;
    params.dt = 1e-3;
    params.datadir = "data_raw/";
    params.frame = -1;  // The id of a particle in which frame we are. -1 will
                        // mean that the frame will be always inertial and will not move
                        // together with some particle.
    params.getConservedQ = true;
    params.range = 1000;

    for (unsigned i = 0; i < expids.size(); i++) {
        params.expid = expids[i];
        switch (expids[i]) {
            case 1:
                experimentKepler1 (params);
                break;
            case 2:
                // No need to write these during this experiment
                params.getConservedQ = false;
                experimentTestMasses (params);
                break;
            case 3:
                experimentParabolic (params);
                break;
            case 4:
                // No need to write these during this experiment
                params.getConservedQ = false;
                experimentParabolicWithTestMasses (params);
                break;
            default: 
                std::cout << "WARNING: The id " << expids[i] << " does not exist" 
                    << std::endl;
                break;
        }
    }

    return 0;
}

int experimentKepler1 (grav::experimentContainer params) {
    // Set some required parameters
    params.dt = 1e-3;
    params.tMax = 1e3;

    // Introduce the particle array:
    grav::ParticleSystem particles;
    std::vector<int> idx (2,0);

    // Introduce 2 large gravitating masses and store the indexes in the ParticleSystem
    // array, so that we can later transform between frames of different particles:
    idx[0] = particles.addParticle(1, cav::Vec3(0,0,0), cav::Vec3(0,0,0));
    idx[1] = particles.addParticle(1, cav::Vec3(3,0,0), cav::Vec3(0,pow(2.0/3 - 1.0/7,0.5),0));

    params.frame = idx[0];

    return experimentWrapper (particles, params);
}

int experimentTestMasses (grav::experimentContainer params) {
    // Introduce the particle array:
    grav::ParticleSystem particles;
    std::vector<int> idx (1,0);

    //{{{ The experiment 2:
    params.tMax = 40;
    unsigned pDensity = 12,
             pRingNumber = 16,
             pMaxDensity = pDensity*4;
    double pMinRadius = 2, pMaxRadius=6;

    // We have one gravitating mass and lots of test masses.
    idx[0] = particles.addParticle(1, cav::Vec3(0,0,0), cav::Vec3(0,0,0));
    
    // Set the frame of reference
    params.frame = idx[0];

    // Add the test particles to the array.
    // See the details for the velocity calculation in the Subsection
    // \ref{subsec:velocity} on page \pref{subsec:velocity}.
    for (unsigned i = 0; i < pRingNumber; i ++) {
        double r = pMinRadius + (pMaxRadius-pMinRadius)/pRingNumber*i + 0.001;
        unsigned n = pDensity + (pMaxDensity-pDensity)/pRingNumber*i;
        particles.addTestParticles(idx[0], r, params.G*1, n);
    }
    
    return experimentWrapper (particles, params);
}

int experimentParabolic (grav::experimentContainer params) {
    // Introduce the particle array:
    grav::ParticleSystem particles;
    std::vector<int> idx (2,0);
    
    // Set the stuff
    params.tMax = 700;
    double phi = M_PI/4, r0 = 12;

    // Introduce 2 large gravitating masses and store the indexes in the ParticleSystem
    // array, so that we can later transform between frames of different particles:
    idx[0] = particles.addParticle(1, cav::Vec3(0,0,0), cav::Vec3(0,0,0));

    // FIXME: The values below are properly explained in 
    idx[1] = particles.addParticle(1, 2*r0/(1-cos(phi)) * cav::Vec3(cos(phi), sin(phi), 0),
            pow(2*r0,-0.5) * cav::Vec3 ( -sin(phi), cos(phi) - 1, 0));
    
    params.frame = idx[0];

    return experimentWrapper (particles, params);
}

int experimentParabolicWithTestMasses (grav::experimentContainer params) {
    // Introduce the particle array:
    grav::ParticleSystem particles;
    std::vector<int> idx (2,0);
    
    // Set some constants neede later
    params.tMax = 700;
    unsigned pDensity = 12,
             pRingNumber = 16,
             pMaxDensity = pDensity*4;
    double phi = M_PI/4, r0 = 12,
           pMinRadius = 2, pMaxRadius=6;

    // Introduce 2 large gravitating masses and store the indexes in the ParticleSystem
    // array, so that we can later transform between frames of different particles:
    idx[0] = particles.addParticle(1, cav::Vec3(0,0,0), cav::Vec3(0,0,0));
    idx[1] = particles.addParticle(1, 2*r0/(1-cos(phi)) * cav::Vec3(cos(phi), sin(phi), 0),
            pow(2*r0,-0.5) * cav::Vec3 ( -sin(phi), cos(phi) - 1, 0));

    // Add the test particles to the array.
    // See the details for the velocity calculation in the Subsection
    // \ref{subsec:velocity} on page \pref{subsec:velocity}.
    for (unsigned i = 0; i < pRingNumber; i ++) {
        double r = pMinRadius + (pMaxRadius-pMinRadius)/pRingNumber*i + 0.001;
        unsigned n = pDensity + (pMaxDensity-pDensity)/pRingNumber*i;
        particles.addTestParticles(idx[0], r, params.G*1, n);
    }
    
    params.frame = idx[0];

    return experimentWrapper (particles, params);
}

int experimentWrapper (grav::ParticleSystem& particles, grav::experimentContainer params) {
    // Open two files and prepare them for outputting
    std::ofstream position, conserved, eccentricity;
    std::stringstream positionout, constantsout, eccentricityout;

    positionout  << params.datadir << "exp" << params.expid << "/positions";
    position.open(positionout.str().c_str());

    if (params.getConservedQ) {
        // Constructing filenames:
        eccentricityout << params.datadir << "exp" << params.expid << "/eccentricities";
        constantsout << params.datadir << "exp" << params.expid << "/constants.csv";

        // Open files
        eccentricity.open(eccentricityout.str().c_str());
        conserved.open(constantsout.str().c_str());

        if (not (conserved.is_open() and eccentricity.is_open())) {
            std::cout << "One of the files could not have been opened.\n"
                << "The files which were tried are:\n"
                << "\t" << constantsout.str() << "\n"
                << "\t" << eccentricityout.str() << "\n"
                << "Check, whether the required directories are created." << std::endl;

            return 1;
        }
    }

    // Check whether we succeeded in opening the files.
    if (not position.is_open()) {
        std::cout << "One of the files could not have been opened.\n"
                  << "The files which were tried are:\n"
                  << "\t" << positionout.str() << "\n"
                  << "Check, whether the required directories are created." << std::endl;

        return 1;
    }

    // Initialise the output files with some headers
    position << "# This contains all the positions for the investigated system."
             << "# The data from this file should be coupled with the conserved.csv file.\n"
             << "# The shape of the array is: (container size/3, " << particles.size() << ", 3).\n"
             << "# This strategy was found on http://stackoverflow.com/q/3685265\n"
             << "# The structure of the representation is as follows:\n"
             << "#    # Time stamp t = 1s:\n"
             << "#    x components of the position of the particle i\n"
             << "#    y components of the position of the particle i\n"
             << "#    z components of the position of the particle i\n"
             << "#    # New time stamp...\n"
             << "#" << std::endl;

    if (params.getConservedQ) {
        conserved << "t, Energy, J, Jx, Jy, Jz" << std::endl;
        eccentricity << "# This contains all the eccentricty values for the investigated particles.\n"
                     << "# The data from this file should be coupled with the conserved.csv file.\n"
                     << "# The shape of the array is: (container size/4, " << particles.size() << ", 4).\n"
                     << "# Formatting is similar to the position file.\n"
                     << "# The structure of the representation is as follows:\n"
                     << "#    # Time stamp t = 1s:\n"
                     << "#    x components of the eccentricity of the particle i\n"
                     << "#    y components of the eccentricity of the particle i\n"
                     << "#    z components of the eccentricity of the particle i\n"
                     << "#    magnitude of the eccentricity of the particle i\n"
                     << "#    # New time stamp...\n"
                     << "#" << std::endl;
    }

    // We plot 1 point per second
    int N = (int) 1/params.dt;

    // Set the output precission
    std::cout.precision(12);
    // Loop over the calculations
    for (unsigned i=0; params.t < params.tMax; i++) {
        particles.evolveVerlet (params);
        params.t += params.dt;

        // Output physical quanities only at certain points
        if (i % N == 0) {
            // The energy is calculated on demand. The same for other quantities which
            // are not position, mass, velocity or acceleration.
            std::vector<cav::Vec3> X = particles.getPositions();

            if (params.getConservedQ) {
                double E = particles.getTotalEnergy(params);
                cav::Vec3 J = particles.getTotalAngularMomentum();
                std::vector<cav::Vec3> e = particles.getEccentricities(params);

                conserved << params.t << "," << E << ","
                    << J.getLength() << "," << J.x() << "," << J.y() << "," << J.z()
                    << std::endl;

                eccentricity << "#New slice at t = " << params.t;
                for (unsigned j = 0; j < 3; j++) {
                    eccentricity << "\n";
                    for (unsigned k = 0; k < e.size(); k++) {
                        eccentricity << e[k][j] << " ";
                    }
                }
                eccentricity << "\n";
                for (unsigned k = 0; k < particles.size(); k++) {
                    eccentricity << e[k].getLength() << " ";
                }

                // Clear buffer only now, it should be faster this way.
                eccentricity << std::endl;
            }

            position << "#New slice at t = " << params.t;
            for (unsigned j = 0; j < 3; j++) {
                position << "\n";
                for (unsigned k = 0; k < X.size(); k++) {
                    position << X[k][j] << " ";
                }
            }

            // Clear buffer only now, it should be faster this way.
            position << std::endl;
            if (params.getConservedQ) {
                eccentricity << std::endl;
            }
        }
    }
    position.close();
    if (params.getConservedQ) {
        conserved.close(), eccentricity.close();
    }

    std::cout << "Calculations for the experiment " << params.expid << " are done." 
              << std::endl;

    return 0;
}

int testMain () {
    std::cout << "Testing framework for the project." << std::endl;
    int count = 0, failed = 0;

    test::addTestParticles(count, failed);
    test::setAcceleration(count, failed);

    std::cout << "\nPassed/Total: " << count - failed << "/" << count << std::endl;
    if (failed !=0) {
        return 1;
    } else {
        return 0;
    }
}


// vim: tw=88:foldmethod=marker
