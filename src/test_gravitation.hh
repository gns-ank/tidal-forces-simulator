#ifndef __TEST_GRAVITATION_HH_X_
#define __TEST_GRAVITATION_HH_X_

namespace test{
    void addTestParticles(int&, int&);
    void setAcceleration (int&, int&);
}

#endif
