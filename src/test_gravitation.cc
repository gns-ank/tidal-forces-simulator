#include <iostream>
#include <cmath>
#include "vec3.hh"
#include "gravitation.hh"

// Use the constants from the cmath library
#define _USE_MATH_DEFINES

namespace test {
    void addTestParticles (int& count, int& failed) {
        // Some physics constants: G --- working in pseudo-natural units.
        double P_G = 1;
        const unsigned N = 16; // Number of test particles
        
        std::cout << "Testing addTestParticles method: ";

        // Introduce the particle array:
        grav::ParticleSystem particles;

        int idx = particles.addParticle (1, cav::Vec3(0,0,0), cav::Vec3(0,0,0));

        // Add the test particles to the array.
        // See the details for the velocity calculation in the Subsection
        // \ref{subsec:velocity} on page \pref{subsec:velocity}.
        particles.addTestParticles(idx, 2, P_G*1, N);

        if (N+1==particles.size()) {
            std::cout << "Passed";
        } else {
            std::cout << "Failed";
            failed += 1;
        }
        
        std::cout << std::endl;
        count++;
    }

    void setAcceleration (int& count, int& failed) {
        // Some physics constants: G --- working in pseudo-natural units.
        double P_G = 1;

        std::cout << "Testing setAcceleration method: ";

        // Introduce the partile array:
        grav::ParticleSystem particles;
        grav::experimentContainer params;
        params.G = 1;
        params.frame = -1;

        // Add the test particles to the array.
        // See the details for the velocity calculation in the Subsection
        // \ref{subsec:velocity} on page \pref{subsec:velocity}.
        particles.addParticle(1, cav::Vec3 (10,0,0), cav::Vec3 (0,0,0));
        particles.addParticle(10, cav::Vec3 (-3,0,0), cav::Vec3 (0,0,0));

        // Calculate the acceleration
        particles.setAcceleration (params);

        std::vector<cav::Vec3> a_theo;
        a_theo.push_back(P_G * cav::Vec3 (-1,0,0) / pow (13,2) * 10);
        a_theo.push_back(P_G * cav::Vec3 (1,0,0) / pow (13,2) * 1);

        // The parameters means that we will not choose any particle as our frame of
        // reference.
        std::vector<cav::Vec3> a = particles.getAccelerations();

        // This works because I am checking whether the doubles are equal using the
        // epsilon value. I suppose that this should be quite rigid, as lots of
        // people on the internet are using it.
        if (a_theo[0] == a[0] and a_theo[1] == a[1]) {
            std::cout << "Passed";
        } else {
            std::cout << "Failed" << std::endl
                      << "The accelerations didn't match: " << a_theo[0] << " != " << a[0]
                      << "                            and " << a_theo[1] << " != " << a[1];
            failed ++;
        }

        std::cout << std::endl;
        count++;
    }
}
