#! /usr/bin/env python3

# Do not use an X backend
from matplotlib import use, rcParams, rc
use('Qt4Agg')
rc('text', usetex=True) #use latex for text
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


# Import the required matplotlib interfaces
import matplotlib.pyplot as plt
import numpy as np

m = 1/2
M = 1
G = 1
x0 = 0
x1 = -12

x = np.linspace(-10,-1,255)
y = -M*G/abs(x-x0) - m*G/abs(x-x1)

# The maximum is here:
x_max = x[0]; y_max = y[0]
for i in range(1, len(y)):
  if y_max < y[i]:
    x_max = x[i]; y_max = y[i]
  else:
    break

E_dens = G*M/abs(2*x - 2*x0) + -M*G/abs(x-x0)

print (x_max,y_max)

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(x,y)
ax.plot(x,E_dens)

plt.show()
