#! /usr/bin/env python3

# Do not use an X backend
from matplotlib import use, rcParams, rc
use('Agg')
rc('text', usetex=True) #use latex for text
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

# Import the required matplotlib interfaces
from matplotlib.mlab import csv2rec
import matplotlib.pyplot as plt
import numpy as np

# data directory
data_dir = "data_raw/"
expid = '1'
print('Making plots for the experiment '+ expid)

# Import the data
position = np.loadtxt(data_dir + 'exp' + expid + '/positions')
eccentricity = np.loadtxt(data_dir + 'exp' + expid + '/eccentricities')
constants = csv2rec(data_dir + 'exp' + expid + '/constants.csv')

# This method was taken from:
# http://stackoverflow.com/questions/3685265
position = position.reshape((position.shape[0]/3,3,position.shape[1]))
position = position.swapaxes(1,2)
eccentricity = eccentricity.reshape((eccentricity.shape[0]/4,4,eccentricity.shape[1]))
eccentricity = eccentricity.swapaxes(1,2)

#{{{ Trajectory
fig = plt.figure(figsize=(4,3))
ax = plt.subplot(111, aspect=1)

ax.plot([0],[0], 'or')
ax.plot(position[:,1,0], position[:,1,1], 'b-', lw=1.5)

# tidy up the ticks and spines
ax.set_title(r'Elliptic orbit of a partcile in a central potential')
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')

plt.grid()

# tight layout
plt.tight_layout()

# save the figure
plt.savefig('report/figs/Kepler1.pdf', bbox_inches='tight')
#}}}
#{{{ Energy
fig = plt.figure(figsize=(4,4))
ax1 = plt.subplot(311)
ax1.plot(constants['t'], constants['energy'], 'r-', label=r'$E$', lw=1.5)
ax1.yaxis.major.formatter.set_powerlimits((0,0)) 

# tidy up the ticks and spines
#ax1.set_xlabel(r'$t$')
ax1.set_ylabel(r'$E$', fontsize=10)
ax1.set_xticklabels([])

ax1.set_ylim([-0.072, -0.071])
#ax1.yaxis.set_major_locator(MaxNLocator(4))

ax1.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize="9",
    title='Total Energy')

ax2 = plt.subplot(312)

ax2.plot(constants['t'], constants['j'] ,  'k-' , label=r'$|\mathbf{J}|$', lw=1.5)
ax2.plot(constants['t'], constants['jx'], 'r--', label=r'$J_x$')
ax2.plot(constants['t'], constants['jy'], 'g--', label=r'$J_y$')
ax2.plot(constants['t'], constants['jz'], 'b--', label=r'$J_z$')

#Legend
ax2.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize="9",
    title='Ang. Momentum')

# tidy up the ticks and spines
ax2.set_ylabel(r'$\mathbf{J}$', fontsize=11)
ax2.set_xticklabels([])
abc = constants['j'].max()
ax2.set_ylim([-0.05*abc, 1.1*abc])

ax3 = plt.subplot(313)

ax3.plot(constants['t'], eccentricity[:,1,3], 'k-' , label=r'$|\mathbf{e}|$', lw=1.5)
ax3.plot(constants['t'], eccentricity[:,1,0], 'r--', label=r'$e_x$')
ax3.plot(constants['t'], eccentricity[:,1,1], 'g--', label=r'$e_y$')
ax3.plot(constants['t'], eccentricity[:,1,2], 'b--', label=r'$e_z$')
abc = eccentricity[:,1,3].max()
ax3.set_ylim([-0.1*abc, 1.1*abc])

# Legend
ax3.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize="9",
    title='Eccentricity')

# tidy up the ticks and spines
ax3.set_xlabel(r'$t$', fontsize=11)
ax3.set_ylabel(r'$\mathbf{e}$', fontsize=11)

# tight layout
plt.tight_layout()

# save
plt.savefig('report/figs/Kepler1-conserved.pdf', bbox_inches='tight')
#}}}

# vim: tw=88 fdm=marker
