#! /usr/bin/env python3

# This will draw the figure in the report.

#{{{ Setup
# Do not use an X backend
from matplotlib import use, rcParams, rc
use('Agg')
rc('text', usetex=True) #use latex for text
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

# Import the required matplotlib interfaces
import matplotlib.pyplot as plt
import matplotlib.patches as pts
import numpy as np
#}}}

# Some constants
r0 = 10
e = 0.7

phi = np.linspace(0,2*np.pi,500)
r = r0 * ( 1 + e ) / ( 1 - e * np.cos(phi))
Phi = np.pi/3
R = r0 * ( 1 + e ) / ( 1 - e * np.cos(Phi))

x = r*np.cos(phi)
y = r*np.sin(phi)

# tangent equations
x_tg = np.linspace(1.1*R*(e*np.cos(Phi)-1)/(e-np.cos(Phi)), R*2.3*np.cos(Phi), 3)
y_tg = (e - np.cos(Phi))/np.sin(Phi) * x_tg + R*(1-e*np.cos(Phi))/np.sin(Phi)

fig = plt.figure(figsize=(6,5))
ax = fig.add_subplot(111)

ax.plot(x,y, 'b-', lw=2.5)
ax.plot(x_tg, y_tg, 'r--', lw=2.0)
# Add a red dot on the intersection
ax.plot([R*np.cos(Phi)], [R*np.sin(Phi)], 'o', lw=1.5, mfc='r', mec='r')

#{{{ Annotations
an = [0,1,2,3]
an[0] = ax.add_patch(pts.Wedge((0,0), 0.15*R,  0, Phi*180/np.pi,  width=0.10, color='k'))
an[1] = ax.add_patch(pts.Wedge((R*(e*np.cos(Phi)-1)/(e-np.cos(Phi)),0), 0.8*R,  0, 
                  np.arctan((e-np.cos(Phi))/np.sin(Phi))*180/np.pi,  width=0.10, color='k'))

ax.annotate(r'$\phi$',
    xy=(1.4, 0.5), xycoords=an[0],
    xytext=(0, 0), textcoords='offset points',
    )
ax.annotate(r'$\alpha$',
    xy=(3, 0.3), xycoords=an[1],
    xytext=(0, 0), textcoords='offset points',
    )

ax.annotate('',
    xy=(R*np.cos(Phi),R*np.sin(Phi)), xycoords='data',
    xytext=(0, 0), textcoords='data',
    arrowprops=dict(arrowstyle="-|>",
      color='red',
      lw=1.5,
      connectionstyle="arc3"),
    )
ax.annotate(r'$R$',
    xy=(np.cos(Phi)*R/2 - 4.3, np.sin(Phi)*R/2), xycoords='data',
    xytext=(0, 0), textcoords='offset points',
    )
ax.annotate(r'$r_0$',
    xy=(-6, -3.5), xycoords='data',
    xytext=(0, -2), textcoords='offset points',
    )

ax.annotate(
    r'$'
    r'r = r_0 \cfrac{1 + e}{1 - e\cos{\phi}}'
    r'$',
    xy=(0.15, 0.85), xycoords="axes fraction",
    va="center", ha="center",
    bbox=dict(boxstyle="round", fc="w"))
#}}}
#{{{ Tidy up the plotting area
# Set the spines
ax.set_xlim([1.1*x_tg.min(),1.1*x.max()])
ax.set_ylim([1.1*y.min(),1.1*y_tg.max()])

for direction in ['right', 'top']:
  ax.spines[direction].set_color('none')
for direction in ['bottom', 'left']:
  ax.spines[direction].set_position(('data',0))
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

an[2] = plt.arrow(0,1.1*y_tg.max()-1,0,1, fc='k', ec='k', zorder=99,
          shape='full', lw=None, length_includes_head=True, head_width=1.0)
an[3] = plt.arrow(1.1*x.max()-1,0,1,0, fc='k', ec='k', zorder=99,
          shape='full', lw=None, length_includes_head=True, head_width=1.0)

ax.set_aspect(1)

# Remove the ticks
ax.set_xticks([])
ax.set_yticks([])

# Set the axes labels
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$', rotation=0)
ax.yaxis.set_label_coords(0.65, 0.95)
ax.xaxis.set_label_coords(1, 0.44)
#}}}

plt.savefig('report/figs/eccentric-traj.pdf', bbox_inches='tight')

# vim: tw=88 fdm=marker
