#! /usr/bin/env python3

# Do not use an X backend
from matplotlib import use, rcParams, rc
use('Agg')
rc('text', usetex=True) #use latex for text
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

# Import the required matplotlib interfaces
from matplotlib.mlab import csv2rec
import matplotlib.pyplot as plt
import numpy as np

# data directory
data_dir = "data_raw/"
expid = '2'
print('Making plots for the experiment '+ expid)

# Import the data
position = np.loadtxt(data_dir + 'exp' + expid + '/positions')

# This method was taken from:
# http://stackoverflow.com/questions/3685265
position = position.reshape((position.shape[0]/3,3,position.shape[1]))
position = position.swapaxes(1,2)

#{{{ Trajectory
fig = plt.figure(figsize=(3.8,3.8))
ax1 = fig.add_subplot(111, aspect=1)

colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k']

pDensity = 12
pMaxDensity = pDensity * 4
pRingnumber = 16

ax1.plot([0],[0], 'or')

k = 0
for i in range(0,pRingnumber):
  for j in range(pDensity + int((pMaxDensity - pDensity)/pRingnumber)*i):
    k += 1
    ax1.plot(position[:,k,0], position[:,k,1], colors[(i-4)%len(colors)] + '-', ms=0.90, mew=0)

# tidy up the ticks and spines
ax1.set_title(r'Orbits of the test particles')
ax1.set_xlabel(r'$x$')
ax1.set_ylabel(r'$y$')

ax1.set_xlim([-6.2,6.2])
ax1.set_ylim([-6.2,6.2])
plt.grid()

# tight layout
plt.tight_layout()

# save the figure
plt.savefig('report/figs/TestParticles.pdf', bbox_inches='tight')
#}}}

# vim: tw=88 fdm=marker
