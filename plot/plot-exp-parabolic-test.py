#! /usr/bin/env python2.7

# Do not use an X backend
from matplotlib import use, rcParams, rc
use('Agg')

# Import the required matplotlib interfaces
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

# data directory
data_dir = "data_raw/"
expid = '4'
print('Making video for the experiment '+ expid)

# Import the data
position = np.loadtxt(data_dir + 'exp' + expid + '/positions')

# This method was taken from:
# http://stackoverflow.com/questions/3685265
position = position.reshape((position.shape[0]/3,3,position.shape[1]))
position = position.swapaxes(1,2)

#{{{ Trajectory
fig = plt.figure(figsize=(8,8))
ax1 = fig.add_subplot(111, aspect=1)

# tidy up the ticks and spines
ax1.set_title(r'Final simulation with initial conditions as described in the report')
ax1.set_xlabel(r'$x$')
ax1.set_ylabel(r'$y$')

ax1.plot([0],[0], 'or')

lineParabola = []
t, = ax1.plot([],[],'b-', lw=1.5)
lineParabola.append(t)
t, = ax1.plot([],[],'bo', lw=1.5)
lineParabola.append(t)
lim = 1.1*position[:,1,0:2].max()
ax1.set_xlim([-lim,lim])
ax1.set_ylim([-lim,lim])

colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k']

pDensity = 12
pMaxDensity = pDensity * 4
pRingnumber = 16

lineTest = []
for i in range(0,pRingnumber):
  for j in range(pDensity + int((pMaxDensity - pDensity)/pRingnumber)*i):
    t, = ax1.plot([],[], colors[(i-4)%len(colors)] + 'o', ms=0.90, mew=0)
    lineTest.append(t)

def animate(i):
  lineParabola[0].set_data(position[:i+1,1,0], position[:i+1,1,1])
  lineParabola[1].set_data(position[i,1,0], position[i,1,1])
  for j in range(len(lineTest)):
    lineTest[j].set_data(position[i,2+j,0], position[i,2+j,1])

  return lineTest, lineParabola

ani = animation.FuncAnimation(fig, animate, np.arange(1, position.shape[0]),
    interval=25)

# save the figure
ani.save("perturbation1.mp4", fps=8, dpi=150, writer='ffmpeg', extra_args=['-vcodec', 'libx264'])
#plt.show()
#}}}

# vim: tw=88 fdm=marker
